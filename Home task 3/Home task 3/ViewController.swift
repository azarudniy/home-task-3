//
//  ViewController.swift
//  Home task 3
//
//  Created by Александр Зарудний on 9/20/21.
//

import UIKit
import SnapKit
import Contacts
import ContactsUI
import CoreData

class ViewController: UIViewController, CNContactPickerDelegate, GetContextProtocol {
    
    func getContext(container name: String) -> NSManagedObjectContext {
        let persistentContainer: NSPersistentContainer = {
            let container = NSPersistentContainer(name: name)
            container.loadPersistentStores(completionHandler: { (storeDescription, error) in
                if let error = error as NSError? {
                    fatalError("Some error \(error)")
                }
            })
            return container
        }()
        return persistentContainer.viewContext
    }
    
    //MARK: ~buttons
    lazy var chooseContactButton: UIButton = {
        let button = UIButton()
        button.setTitle("Выбрать контакт", for: .normal)
        button.backgroundColor = .darkGray
        button.addTarget(self, action: #selector(chooseContact), for: .touchUpInside)
        return button
    }()
    
    lazy var showContactsButton: UIButton = {
        let button = UIButton()
        button.setTitle("Показать контакты", for: .normal)
        button.backgroundColor = .darkGray
        button.addTarget(self, action: #selector(showContact), for: .touchUpInside)
        return button
    }()
    
    lazy var showPhoneNumberButton: UIButton = {
        let button = UIButton()
        button.setTitle("Показать номер", for: .normal)
        button.backgroundColor = .darkGray
        button.addTarget(self, action: #selector(showPhoneNumber), for: .touchUpInside)
        return button
    }()
    
    //MARK: ~viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        self.navigationController?.navigationBar.isHidden = true
        view.addSubview(chooseContactButton)
        view.addSubview(showContactsButton)
        view.addSubview(showPhoneNumberButton)
        setConstraints()
        
        CNContactStore().requestAccess(for: .contacts) { (success, error) in
            guard success else {
                return
            }
        }
    }
    
    //MARK: ~present alert save successfully
    @objc func showAlertSaveSuccess() {
        let alertView = UIAlertController(title: "Сохранение успешно", message: "", preferredStyle: .alert)
        alertView.addAction(UIAlertAction(title: "ОК", style: .destructive, handler: nil))
        DispatchQueue.main.asyncAfter(deadline: .now(), execute: {
            self.present(alertView, animated: true, completion: nil)
        })
    }
    
    //MARK: ~constraints buttons
    private func setConstraints() {
        showContactsButton.snp.makeConstraints {
            $0.center.equalToSuperview()
            $0.width.equalTo(200)
        }
        chooseContactButton.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.bottom.equalTo(showContactsButton.snp.top).inset(-20)
            $0.width.equalTo(showContactsButton.snp.width)
        }
        showPhoneNumberButton.snp.makeConstraints {
            $0.centerX.equalToSuperview()
            $0.top.equalTo(showContactsButton.snp.bottom).offset(20)
            $0.width.equalTo(showContactsButton.snp.width)
        }
    }
    
    //MARK: ~show system Contacts
    @objc func chooseContact(sender: UIButton) {
        let contactsViewController = CNContactPickerViewController()
        contactsViewController.delegate = self
        contactsViewController.modalPresentationStyle = .fullScreen
        self.present(contactsViewController, animated: true, completion: nil)
        
    }
    
    //MARK: ~show saved contacts
    @objc func showContact(sender: UIButton) {
        let showSavedContactsControler = SavedContactsViewController()
        showSavedContactsControler.delegate = self
        navigationController?.pushViewController(showSavedContactsControler, animated: true)
        
    }
    
    //MARK: ~show saved phone number
    @objc func showPhoneNumber(sender: UIButton) {
        let alertView = UIAlertController(title: "", message: "", preferredStyle: .alert)
        alertView.addAction(UIAlertAction(title: "ОК", style: .destructive, handler: nil))
        if let number = UserDefaults.standard.object(forKey: "phone") {
            alertView.title = "Номер телефона: "
            alertView.message = number as? String
        } else {
            alertView.title = "Сохранённых номеров нет"
        }
        self.present(alertView, animated: true, completion: nil)
    }
    
    //MARK: ~save contact to Data Core
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        
        let context = getContext(container: "ContactModel")
        
        do {
            let fetch: NSFetchRequest<SimpleContact> = SimpleContact.fetchRequest()
            let objects = try context.fetch(fetch)
            var flag = true
            
            for contactCoreData in objects {
                if contact.phoneNumbers[0].value.stringValue == contactCoreData.phoneNumber {
                    flag = false
                    break
                }
            }
            
            if flag {
                let simpleContact = SimpleContact(context: context)
                simpleContact.phoneNumber = contact.phoneNumbers[0].value.stringValue
                simpleContact.name = contact.givenName
                simpleContact.lastName = contact.familyName
                if !contact.emailAddresses.isEmpty {
                    simpleContact.e_mail = contact.emailAddresses[0].value as String
                } else {
                    simpleContact.e_mail = ""
                }
                try context.save()
                showAlertSaveSuccess()
            }
                    
        } catch {
            fatalError("Error is \(error as NSError)")
        }
    }

}

