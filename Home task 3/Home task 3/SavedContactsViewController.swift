//
//  SavedContactsViewController.swift
//  Home task 3
//
//  Created by Александр Зарудний on 9/27/21.
//

import UIKit
import CoreData

class SavedContactsViewController: UITableViewController {
    var delegate: GetContextProtocol?
    var arrayContacts = [SimpleContact]()
    let cellID = "cellID"
    
    //MARK: ~viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        
        let context = delegate?.getContext(container: "ContactModel")
        do {
            let fetch: NSFetchRequest<SimpleContact> = SimpleContact.fetchRequest()
            if let objects = try context?.fetch(fetch) {
                arrayContacts = objects
            }
        } catch  {
        }
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellID)

    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayContacts.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath)
        cell.textLabel?.text = arrayContacts[indexPath.row].phoneNumber
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        UserDefaults.standard.setValue(cell?.textLabel?.text, forKey: "phone")
        self.navigationController?.popViewController(animated: true)
    }
}
