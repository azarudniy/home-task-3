//
//  GetContextProtocol.swift
//  Home task 3
//
//  Created by Александр Зарудний on 9/27/21.
//

import Foundation
import CoreData

protocol GetContextProtocol {

    func getContext(container name: String) -> NSManagedObjectContext
}
